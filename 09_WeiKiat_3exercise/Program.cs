﻿using System;

namespace _WeiKiat_3exercise
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int i = 4;
            int j = 2;
            int result1;
            int result3;

            result1 = (i + j);

            Console.WriteLine("The sum of i and j is: " + result1);

            int input1, input2, result2;

            Console.Write("Input the first number to add: ");
            input1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Input the second number to add: ");
            input2 = Convert.ToInt32(Console.ReadLine());

            result2 = input1 + input2;

            Console.WriteLine("The result is: " + result2);

            result3 = (i / j);

            Console.WriteLine("if i is divided by j, the result is: " + result3);
        }
    }
}
